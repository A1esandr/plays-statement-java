package ru.agilix.plays;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class PlaysStatementTests {

    @Test
    public void emptyStatement() {
        Statement stmt = new Statement();
        Invoice invoice = new Invoice("Some customer");
        Map<String, Play> plays = new HashMap<>();

        String result = stmt.statement(invoice, plays);

        assertEquals("Statement for Some customer\n" +
                "Amount owed is $0.00\n" +
                "You earned 0 credits\n", result);
    }

    @Test
    public void statementHasOneTragedyPlay() {
        Invoice invoice = new Invoice("Some customer");
        invoice.addPerformance(new Performance("hamlet", 55));
        Statement stmt = new Statement();
        Map<String, Play> plays = new HashMap<>();
        plays = addPlay(plays, new Play("hamlet", "Hamlet", "tragedy"));

        String result = stmt.statement(invoice, plays);

        assertEquals("Statement for Some customer\n" +
                "  Hamlet: $650.00 (55 seats)\n" +
                "Amount owed is $650.00\n" +
                "You earned 25 credits\n", result);

    }

    @Test
    public void oneTragedyAudienceBelow30() {
        Invoice invoice = new Invoice("Some customer");
        invoice.addPerformance(new Performance("hamlet", 29));
        Statement stmt = new Statement();
        Map<String, Play> plays = new HashMap<>();
        plays = addPlay(plays, new Play("hamlet", "Hamlet", "tragedy"));

        String result = stmt.statement(invoice, plays);

        assertEquals("Statement for Some customer\n" +
                "  Hamlet: $400.00 (29 seats)\n" +
                "Amount owed is $400.00\n" +
                "You earned 0 credits\n", result);
    }

    @Test
    public void oneComedyPlay() {
        Invoice invoice = new Invoice("Some customer");
        invoice.addPerformance(new Performance("as-like", 35));
        Statement stmt = new Statement();
        Map<String, Play> plays = new HashMap<>();
        plays = addPlay(plays, new Play("as-like", "As You Like It", "comedy"));

        String result = stmt.statement(invoice, plays);

        assertEquals("Statement for Some customer\n" +
                "  As You Like It: $580.00 (35 seats)\n" +
                "Amount owed is $580.00\n" +
                "You earned 12 credits\n", result);
    }

    @Test
    public void oneComedyPlayAudienceBelow20() {
        Invoice invoice = new Invoice("Some customer");
        invoice.addPerformance(new Performance("as-like", 19));
        Statement stmt = new Statement();
        Map<String, Play> plays = new HashMap<>();
        plays = addPlay(plays, new Play("as-like", "As You Like It", "comedy"));

        String result = stmt.statement(invoice, plays);

        assertEquals("Statement for Some customer\n" +
                "  As You Like It: $357.00 (19 seats)\n" +
                "Amount owed is $357.00\n" +
                "You earned 3 credits\n", result);
    }

    @Test(expected = RuntimeException.class)
    public void unknownPlayTypeShouldThrowException() {
        Invoice invoice = new Invoice("Some customer");
        invoice.addPerformance(new Performance("mario", 35));
        Statement stmt = new Statement();
        Map<String, Play> plays = new HashMap<>();
        plays = addPlay(plays, new Play("mario", "Super Mario Brothers", "arcade"));

        String result = stmt.statement(invoice, plays);
    }

    private Map<String, Play> addPlay(Map<String, Play> plays, Play play) {

        plays.put(play.getID(), play);
        return plays;
    }
}