package ru.agilix.plays;

import java.util.ArrayList;
import java.util.List;

public class Invoice {
    private final String customer;
    private List<Performance> performanceList = new ArrayList<>();

    public Invoice(String customer) {
        this.customer = customer;
    }

    public String getCustomer() {
        return customer;
    }

    public List<Performance> getPerformances() {
        return performanceList;
    }

    public void addPerformance(Performance perf) {
        performanceList.add(perf);
    }

}
