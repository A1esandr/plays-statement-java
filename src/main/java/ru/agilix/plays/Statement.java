package ru.agilix.plays;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Map;


public class Statement {

    private Map<String, Play> plays;

    public String statement(Invoice invoice, Map<String, Play> plays) {
        this.plays = plays;
        Locale locale = new Locale("en", "US");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
        String result = "Statement for " + invoice.getCustomer() + "\n";
        for (Performance performance : invoice.getPerformances()) {
            result += "  " + playFor(performance).getName() + ": " + format(amountFor(performance), fmt) + " (" + performance.getAudience() + " seats)\n";
        }
        result += "Amount owed is " + format(totalAmount(invoice), fmt) + "\n";
        result += "You earned "+ totalVolumeCredits(invoice) + " credits\n";
        return result;
    }

    private int totalAmount(Invoice invoice) {
        int result = 0;
        for (Performance performance : invoice.getPerformances()) {
            result += amountFor(performance);
        }
        return result;
    }

    private int totalVolumeCredits(Invoice invoice) {
        int result = 0;
        for (Performance performance : invoice.getPerformances()) {
            result += getVolumeCredits(performance);
        }
        return result;
    }

    private int getVolumeCredits(Performance performance) {
        // add volume credits
        int result = volumeCreditFor(performance);
        // add extra credit for every ten comedy attendees
        if (playFor(performance).getType() == "comedy")
            result += Math.floor(performance.getAudience() / 5);
        return result;
    }

    private String format(int totalAmount, NumberFormat fmt) {
        return fmt.format(totalAmount / 100);
    }

    private int volumeCreditFor(Performance perf) {
        return Math.max(perf.getAudience() - 30, 0);
    }

    private Play playFor(Performance perf) {
        return this.plays.get(perf.getPlayID());
    }

    private int amountFor(Performance performance) {
        int result;
        switch (playFor(performance).getType()) {
            case "tragedy":
                result = 40000;
                if (performance.getAudience() > 30) {
                    result += 1000 * (performance.getAudience() - 30);
                }
                break;
            case "comedy":
                result = 30000;
                if (performance.getAudience() > 20) {
                    result += 10000 + 500 * (performance.getAudience() - 20);
                }
                result += 300 * performance.getAudience();
                break;
            default:
                throw new RuntimeException("unknown type: " + playFor(performance).getType());
        }
        return result;
    }

}
